<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $errorFloatOrInteger = 'The value must be an integer or a float';

        $builder
            ->add('orderId')
            ->add('amount', 'number', ['scale' => 2, 'invalid_message' => $errorFloatOrInteger])
            ->add('tax', 'number', ['scale' => 2, 'invalid_message' => $errorFloatOrInteger])
            ->add('shipping', 'number', ['scale' => 2, 'invalid_message' => $errorFloatOrInteger])
            ->add('processingFee', 'number', ['scale' => 2, 'invalid_message' => $errorFloatOrInteger])
            ->add(
                'currency', 'choice', [
                    'choices' => ['EUR' => 'Euro', 'USD' => 'Dollar'],
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'TestBundle\Entity\Order'
            ]
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'testbundle_order';
    }
}
