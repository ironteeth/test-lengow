<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Order Api controller. Support Json an Yaml
 *
 */
class ApiOrderController extends Controller
{
    /**
     * Lists all Order entities in the desired format.
     *
     */
    public function allAction($_format)
    {
        $orders = $this->get('test.repository.order')->findAll();

        $serializedEntity = $this->get('serializer')->serialize($orders, $_format);

        return new Response($serializedEntity);
    }

    /**
     * Display an Order entity with the given id in the desired format.
     *
     */
    public function getAction($id, $_format)
    {
        $order = $this->get('test.repository.order')->findOneBy(['orderId' => $id]);

        if (is_null($order)) {

            if ($_format == 'json') {
                return new JsonResponse([], 404);
            } else {
                return new Response(null, 404);
            }
        }

        $serializedEntity = $this->get('serializer')->serialize($order, $_format);

        return new Response($serializedEntity);
    }
}