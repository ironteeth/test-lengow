<?php

namespace TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Source\Entity;

use TestBundle\Entity\Order;

/**
 * Order controller. Index and create action only
 *
 */
class OrderController extends Controller
{
    /**
     * Lists all Order entities.
     *
     */
    public function indexAction()
    {
        $source = new Entity('TestBundle:Order');

        $grid = $this->get('grid');

        $grid->setSource($source);

        return $grid->getGridResponse('TestBundle:Order:index.html.twig');
    }

    /**
     * Creates a new Order entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Order();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('order_index'));
        }

        return $this->render(
            'TestBundle:Order:new.html.twig', [
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Creates a form to create a Order entity.
     *
     * @param Order $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Order $entity)
    {
        $form = $this->createForm(
            'testbundle_order', $entity, [
                'action' => $this->generateUrl('order_create'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Create']);

        return $form;
    }

    /**
     * Displays a form to create a new Order entity.
     *
     */
    public function newAction()
    {
        $entity = new Order();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'TestBundle:Order:new.html.twig', [
                'entity' => $entity,
                'form' => $form->createView(),
            ]
        );
    }
}
