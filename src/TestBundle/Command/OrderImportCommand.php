<?php

namespace TestBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('order:import')
            ->setDescription('Import des commandes');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Début de l'import des commandes");

        $httpClient = $this->getContainer()->get('lengow_test');
        $httpClient->getOrders();

        $importConnector = $this->getContainer()->get('test.order.import.connector');
        $importConnector->importOrders();
        $ordersCount = $importConnector->getOrdersCount();

        $output->writeln("{$ordersCount} commande(s) importées");
    }
}