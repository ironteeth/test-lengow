<?php

namespace TestBundle\Order;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use TestBundle\Entity\Order;
use Symfony\Component\Finder\Finder;

/**
 * The OrderImportConnector reads xml files in the designed directory and insert orders in the database
 * Xsd validation missing
 *
 */
class OrderImportConnector
{

    /** @var  \SplFileInfo $orderFile */
    private $orderFile;

    private $ordersCount = 0;

    private $importDir;

    /** @var LoggerInterface */
    private $logger;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct($importDir, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->importDir = $importDir;
        $this->logger = $logger;
        $this->em = $em;
    }

    public function importOrders()
    {
        $finder = new Finder();
        $orderFiles = $finder->files()->in($this->importDir);

        foreach ($orderFiles as $orderFile) {

            $this->orderFile = $orderFile;
            $ordersXml = file_get_contents($orderFile);
            $this->readOrderXml($ordersXml);
        }
    }

    public function readOrderXml($ordersXml)
    {
        if ($ordersXml = @simplexml_load_string($ordersXml, 'SimpleXMLElement', LIBXML_NOCDATA)) {
            foreach ($ordersXml->orders as $orderXml) {
                foreach ($orderXml->order as $order) {
                    $this->processOrder($order);
                }
            }
        } else {
            $this->logger->error('Invalid XML File', [$this->orderFile->getPath() . '/' . $this->orderFile->getFilename()]);
            throw new \RuntimeException('Invalid XML File');
        }
    }

    public function processOrder($order)
    {
        $orderRepo = $this->em->getRepository('TestBundle:Order');
        $orderEntity = $orderRepo->findOneBy(['orderId' => $order->order_id]);

        if (is_null($orderEntity)) {
            $newOrder = new Order();
            $newOrder->setOrderId($order->order_id);
            $newOrder->setAmount($order->order_amount);
            $newOrder->setTax($order->order_tax);
            $newOrder->setShipping($order->order_shipping);
            $newOrder->setProcessingFee($order->order_processing_fee);
            $newOrder->setCurrency($order->order_currency);
            $this->em->persist($newOrder);
            $this->em->flush();
            $this->ordersCount++;
        }
    }

    public function getOrdersCount()
    {
        return $this->ordersCount;
    }
}