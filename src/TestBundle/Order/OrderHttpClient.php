<?php

namespace TestBundle\Order;

use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Http client based on curl to grab an xml order file.
 *
 */
class OrderHttpClient
{

    /** @var  Logger */
    private $logger;

    private $urlOrders;

    private $importDir;

    const ORDER_FILE = "orders.xml";

    public function __construct($urlOrders, $importDir, LoggerInterface $logger)
    {
        $this->urlOrders = $urlOrders;
        $this->importDir = $importDir;
        $this->logger = $logger;
    }

    public function getOrders()
    {

        $ch = curl_init($this->urlOrders);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $response = curl_exec($ch);

        if ($response === false) {
            throw new \RuntimeException(curl_error($ch));
        }

        $request_headers = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        $this->logger->debug("[Order Request headers] " . $request_headers);

        $response_header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        $response_header = substr($response, 0, $response_header_size);
        $body = substr($response, $response_header_size);

        file_put_contents($this->importDir . self::ORDER_FILE, $body);

        $this->logger->debug("[Order Response headers] " . $response_header);
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getUrlOrders()
    {
        return $this->urlOrders;
    }

    /**
     * @param mixed $urlOrders
     */
    public function setUrlOrders($urlOrders)
    {
        $this->urlOrders = $urlOrders;
    }

    /**
     * @return mixed
     */
    public function getImportDir()
    {
        return $this->importDir;
    }

    /**
     * @param mixed $importDir
     */
    public function setImportDir($importDir)
    {
        $this->importDir = $importDir;
    }
}