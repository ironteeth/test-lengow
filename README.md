#Test à réaliser

	1. Créer un TestBundle.
	2. Créer un fichier de configuration avec un paramètre url_orders : http://test.lengow.io/orders-test.xml.
	3. Créer une classe permettant d'aller récupérer le flux xml en utilisant le paramètre ci dessus.
	4. Faire que cette classe devienne un service "lengow_test" et que le paramètre "url_orders" et le "logger" soient passés en injection de dépendance. Logger doit être utilisé pour tracer les appels et les retours des téléchargements du fichier xml.
	5. Une fois le fichier récupéré, sauvegarder les commandes dans une BDD en utilisant une entité et doctrine. Vous n'êtes pas obligé de sauvegarder tous les champs, mais seulement 4 ou 5. Ne pas sauvegarder une commande si elle est déjà présente en base de donnée.
	6. Afficher les résultats sous forme de tableau en utilisant un bundle de gestion de grid : https://github.com/Abhoryo/APYDataGridBundle.
	7. Créer un formulaire sf2 pour insérer une commande en BDD.
	8. Créer une route du type /api/ pour récupérer toutes les commandes en BDD au format "json".
	9. Créer une route du type /api/%id_order%/ pour récupérer une commande sous format "json".
	10. Offrir la possibilité avec un paramètre d'avoir les commandes au format "yaml".


#Informations

Le test a été réalisé avec php 5.5.9 et mysql 5.5.
L'extension curl de php doit être installée
Bootstrap 3 est utilisé pour le thème du formulaire

#Installation

  `composer install`

  `app/console doctrine:migrations:migrate --no-interaction`

  Il faut aussi donner les droits d'écriture sur le répertoire import à la racine du répertoire d'installation c'est la
  que sera sauvegardé le xml de commandes (sudo chmod a+w import)

#Utilisation

Une commande a été créée pour importer les commandes :

  `app/console order:import`

Ensuite on peut activer le serveur http en console :

  `app/console server:run`

Et aller voir les commandes et en ajouter si nécéssaire sur http://127.0.0.1:8000

#Api

Les commandes sont disponibles en json et yml. Si l'on veut du yaml on remplace simplement json par yml dans l'url :

   `http://127.0.0.1:8000/api/json/orders`
   `http://127.0.0.1:8000/api/yml/orders`

Pour accéder à une seule commande on ajoute son order_id à la fin de l'url :

   `http://127.0.0.1:8000/api/json/orders/123-4567890-1112131`
   `http://127.0.0.1:8000/api/yml/orders/123-4567890-1112131`